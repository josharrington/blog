---
id: 115
title: Using the Kace K1000 VNC Button With Chrome And Firefox
date: 2012-01-23T15:01:20+00:00
author: josh
layout: post
guid: http://blog.arrington.me/?p=115
permalink: /2012/using-the-kace-k1000-vnc-button-with-chrome-and-firefox/
categories:
  - Networking
  - Programming
tags:
  - chrome
  - firefox
  - kace
  - userscript
  - vnc
---
Dell&#8217;s Kace K1000 appliance is a neat little device but it is not yet complete as a product. If you have a managed machine inside the K1000, you will see a small icon that opens your UltraVNC viewer and connects to that computer for remote management. Very handy! Unfortunately, it only works in IE. Bad, programmers, bad!

I wrote a user script for Chrome and Firefox (with Greasemonkey) that will make this work. It involves registering a protocol in your OS of choice (like http://, ftp://, etc) to open your VNC viewer and changing the link on the K1000 to point to that new protocol.

Below is the userscript. Copy it into your favorite text editor, change MYK1000APPLIANCE in the include line to be the location of your K1000 box and save it as named below. You can import it into Chrome by just dragging the file into an open Chrome window.<!--more-->

With Firefox, you will need to install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/). After installation, the Greasemonkey icon should appear in the top right corner of Firefox. Click on the arrow beside the monkey, click &#8220;New User Script.&#8221; Give the script a name (Kace VNC Button) and a namespace (Kace), click OK. If it asks you to choose an editor, navigate to notepad (C:\Windows\Notepad) or some other text editor and the editor will open with a script skeleton already inside. Copy the contents of the file you saved (or the stuff below) and paste it inside the editor, replacing the previous contents. Save the file. The script should be installed.

kacevnc.user.js:

{% highlight javascript %}
// @name           Kace VNC Button
// @namespace      KACE
// @description    Changed IE only VNC button to work with Chrome and Firefox
// @include        http://MYK1000APPLIANCE/adminui/machine.php*
// ==/UserScript==

function addJQuery(callback) {
  var script = document.createElement("script");
  script.setAttribute("src", "http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js");
  script.addEventListener('load', function() {
    var script = document.createElement("script");
    script.textContent = "(" + callback.toString() + ")();";
    document.body.appendChild(script);
  }, false);
  document.body.appendChild(script);
}

// the guts of this userscript
function main() {
  var as = $('a[href="javascript:void(0)"][onclick^="MachineAction"]');
  $.each(as, function(i,a){
		var temp = $(a).attr("onclick");
		temp = temp.substring(temp.indexOf("'")+1);
		var ip = temp.substring(0,temp.indexOf("'"));
		$(a).attr("onclick", "").attr("href", "vnc://" + ip);
	}
  );

}

// load jQuery and execute the main function
addJQuery(main);
{% endhighlight %}

When you navigate to a machine&#8217;s page, nothing should look different except that the button now navigates to: vnc://ipaddress.
  
[<img class="aligncenter size-full wp-image-120" title="2012-01-23_1430" src="http://blog.arrington.me/wp-content/uploads/2012-01-23_1430.png" alt="" width="657" height="200" srcset="https://blog.arrington.me/wp-content/uploads/2012-01-23_1430.png 657w, https://blog.arrington.me/wp-content/uploads/2012-01-23_1430-300x91.png 300w" sizes="(max-width: 657px) 100vw, 657px" />](http://blog.arrington.me/wp-content/uploads/2012-01-23_1430.png)

### Registering the VNC protocol in Windows 7

You will need two files. One you will import into the registry and the other is the script that is run when your browser communicates with vnc:// links.

vnc.reg:

{% highlight plain %}
[HKEY_CLASSES_ROOT\vnc]
@="URL:VNC Connection"
"URL Protocol"=""
[HKEY_CLASSES_ROOT\vnc\DefaultIcon]
@="c:\\progra~1\\UltraVNC\\vncviewer.exe"
[HKEY_CLASSES_ROOT\vnc\shell]
[HKEY_CLASSES_ROOT\vnc\shell\open]
[HKEY_CLASSES_ROOT\vnc\shell\open\command]
@="wscript.exe C:\\WINDOWS\\vnc.js %1"
{% endhighlight %}

vnc.js:

{% highlight javascript %}
var search='vnc://'
//Modify the path to VNC Viewer!
var vncexe='c:\\progra~1\\UltraVNC\\vncviewer.exe'
//WScript.Echo(destination)
destination=destination.replace(search, "")
destination=destination.replace('/', "")
var ws = new ActiveXObject("WScript.Shell")
//WScript.Echo(vncexe + " " + destination)
ws.Exec(vncexe + " " + destination)
{% endhighlight %}

Save vnc.reg to a file and open the file. Windows will ask you to import the registry keys. Do it.

Save vnc.js to C:\windows\vnc.js . If you are not using UltraVNC, you may need to edit these files to point to the correct vncviewer.

Go to a machine&#8217;s inventory page and click on a VNC button. VNC should automatically connect to that machine&#8217;s IP address.

The userscript should be OS agnostic. I have not researched how to implement the url handlers on other OSes.

Tested on:
  
KBOX K1000 Version 5.3.47927
  
Windows 7 32-bit
  
Chrome 16.0.912.75
  
Firefox 9.0.1
  
UltraVNC

### Resources

[Download all the scripts](http://blog.arrington.me/wp-content/uploads/vnc.zip)
  
[Enabling Hyperlinks for Remote Connections in Windows](http://retrohack.com/enabling-hyperlinks-for-remote-connections-in-windows/)
  
[Using jQuery in userscripts](http://stackoverflow.com/questions/2246901/how-can-i-use-jquery-in-greasemonkey-scripts-in-google-chrome)
