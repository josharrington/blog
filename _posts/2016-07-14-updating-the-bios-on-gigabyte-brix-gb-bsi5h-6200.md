---
id: 193
title: Updating the BIOS on Gigabyte BRIX GB-BSi5H-6200
date: 2016-07-14T15:01:59+00:00
author: josh
layout: post
guid: https://blog.arrington.me/?p=193
permalink: /2016/updating-the-bios-on-gigabyte-brix-gb-bsi5h-6200/
categories:
  - Hardware
---
The BRIX that I received from Amazon had a very old BIOS version installed. Newer versions enable better support for certain memory modules and other hardware. Unfortunately, Gigabyte does not provide any instructions at the time of writing on how to flash the BIOS on these machines.

To flash the BIOS on the GB-BSi5H-6200 (and likely all Skylake BRIX units), complete the following.

Requirements:

  * A 256MB or larger USB drive
  * The BIOS update for your system. Make sure to get the correct model. For the GB-BSi5H-6200: <http://www.gigabyte.com/products/product-page.aspx?pid=5690#bios>
  * Rufus to create a bootable USB drive: <https://rufus.akeo.ie/>

Steps:

  1. Launch Rufus and select your USB drive. Leave the defaults.
  
    Note: This will erase all data on the USB drive.
  
    [<img class="size-full wp-image-203 alignnone" src="https://blog.arrington.me/wp-content/uploads/Rufus.png" alt="Rufus" width="379" height="530" />
  
](https://blog.arrington.me/wp-content/uploads/Rufus.png) 
  2. Extract the files from the BIOS zip archive downloaded above to the root of the USB drive. When finished, the USB drive will look something like this:[<img class="size-full wp-image-205 alignnone" src="https://blog.arrington.me/wp-content/uploads/biosdrive.png" alt="biosdrive" width="668" height="324" />](https://blog.arrington.me/wp-content/uploads/biosdrive.png)
  3. Plug your USB drive into the BRIX and boot it up. Press the DEL key as it&#8217;s booting to enter the BIOS setup. You will need to set the following options to be able to boot into the FreeDOS USB Drive. Take note of the settings already in place to revert them later.`Advanced > OS Selection > OS Selection > Windows 7<br />
Advanced > OS Selection > Storage > Legacy`
  4. Reboot the BRIX and press F12 as the system is booting to enter the boot selection screen. Choose your USB Drive from the list, making sure not to select one if it starts with &#8220;UEFI&#8221;
  5. Once booted, you will be at a C:> prompt. Type `flash.bat` and press enter.
  6. The system will then flash your BIOS. This can take up to 15 minutes.
  7. Once finished, you can power off the system, remove the USB drive, and power it back on with a new BIOS.
  8. Revert the settings to their previous values from Step 3.