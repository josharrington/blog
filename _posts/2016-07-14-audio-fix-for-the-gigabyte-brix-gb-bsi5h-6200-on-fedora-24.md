---
id: 195
title: Audio Fix for the Gigabyte BRIX GB-BSi5H-6200 on Fedora 24
date: 2016-07-14T14:35:36+00:00
author: josh
layout: post
guid: https://blog.arrington.me/?p=195
permalink: /2016/audio-fix-for-the-gigabyte-brix-gb-bsi5h-6200-on-fedora-24/
categories:
  - Hardware
  - HTPC
  - Linux
---
I recently purchased the Gigabyte Brix GB-BSi5H-6200 to replace my dated HTPC. It&#8217;s a fantastic little powerhouse NUC-form-factor PC and should be future-proof for 4k video when that time finally arrives. For today, however, it can drive 1080p like nobody&#8217;s business.

However, after installing Fedora 24 (LXDE spin) on it, I found there was an issue with the audio. The sound system did not detect the HDMI port as an audio output. Only the front panel 3.5mm connectors were detected. It works perfectly on Ubuntu 16.04, though! What gives?

After comparing the module options on the Ubuntu install vs Fedora, I discovered Ubuntu had many module options set that enabled the Intel sound driver to work properly.
  
Create the file `/etc/modprobe.d/intel_snd.conf` and add the following lines:

<pre class="brush: bash; title: ; notranslate" title="">option snd_pcsp index=-2
option snd_usb_audio index=-2
option snd_atiixp_modem index=-2
option snd_intel8x0m index=-2
option snd_via82xx_modem index=-2
option snd_atiixp_modem index=-2
option snd_intel8x0m index=-2
option snd_via82xx_modem index=-2
option snd_usb_audio index=-2
option snd_usb_caiaq index=-2
option snd_usb_ua101 index=-2
option snd_usb_us122l index=-2
option snd_usb_usx2y index=-2
option snd_cmipci mpu_port=0x330 fm_port=0x388
option snd_pcsp index=-2
option snd_usb_audio index=-2
</pre>

Then reboot. Done!