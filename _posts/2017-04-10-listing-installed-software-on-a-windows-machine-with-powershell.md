---
id: 218
title: Listing Installed Software on a Windows Machine with PowerShell
date: 2017-04-10T14:52:23+00:00
author: josh
layout: post
guid: https://blog.arrington.me/?p=218
permalink: /2017/listing-installed-software-on-a-windows-machine-with-powershell/
categories:
  - Windows
tags:
  - Powershell
---
Getting a list of installed software programmatically can be a giant hassle on a Windows system. What I wanted was a list that mimics what we see in the control panel. After way too much Googling, I pieced together this solution. 

&nbsp;
{% highlight powershell %}
function Get-InstalledSoftware {
    $paths = @( 
        "\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\"
    )

    if ([Environment]::Is64BitProcess) {
        $paths += "\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\"
    }

    $paths |% {
        if (Test-Path "HKLM:$_") { Get-ChildItem "HKLM:$_" }
        if (Test-Path "HKCU:$_") { Get-ChildItem "HKCU:$_" }
    } |
    ForEach-Object { Get-ItemProperty $_.PSPath } |
    Where-Object {
        $_.DisplayName -and !$_.SystemComponent -and !$_.ReleaseType -and !$_.ParentKeyName -and ($_.UninstallString -or $_.NoRemove)
    } | Sort-Object DisplayName 
}

Get-InstalledSoftware 
{% endhighlight %}


I&#8217;ve used this to verify something was installed from Puppet and to programmatically uninstall ClickOnce apps. Enjoy!
