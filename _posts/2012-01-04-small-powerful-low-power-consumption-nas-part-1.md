---
id: 74
title: Small, Powerful, Low Power Consumption NAS
date: 2012-01-04T22:50:00+00:00
author: josh
layout: post
guid: http://blog.arrington.me/?p=74
permalink: /2012/small-powerful-low-power-consumption-nas-part-1/
categories:
  - Hardware
tags:
  - hardware
  - HP
  - Microserver
  - NAS
  - watts
---
For as long as I have been into technology, my downloads, virtual machines, backups, testing platforms, and general storage have all been scattered about several devices. My newest solution is running my large downloads on a modded [WDTV](http://wdc.com/en/products/homeentertainment/mediaplayers/) to preserve power and keep them centralized. This has become cumbersome as the files have gotten larger and as my needs have grown more complex. I&#8217;ve been imagining the perfect all-in-one solution for the past couple of years, researching new hardware and dreaming bigger and better every few months. After finally moving into a space that would accommodate such a system and finally having the funds saved up, it is time to take the plunge!<!--more-->

### Requirements, Goals, Assumptions

I want to consolidate all of my processing, storage and test beds. Requirements:

  * 4+ 3.5&#8243; Hard Drive Slots and a place for an SSD
  * Enough horsepower to run three virtual machines and other services simultaneously
  * Small enough to fit on a shelf in my closet
  * Quiet enough so it is inaudible from the adjacent room (<30 dB)
  * Zero bright or flashy lights
  * Gigabit Ethernet
  * Low power consumption (<50W)

I&#8217;ve distilled my choices down to three: the all-in-one solution, the low-end custom box, or the higher end custom box.

### The All-In-One

I discovered the [HP Proliant Microserver](http://h10010.www1.hp.com/wwpc/us/en/sm/WF05a/15351-15351-4237916-4237918-4237917-4248009.html) a few months back. It is classified as an &#8220;ultra micro tower.&#8221; It comes in at less than a cubic foot (10.5&#8243; x 8.3&#8243; x 10.2&#8243;) and has space for 4 3.5&#8243; drives and 1 5.25&#8243; [<img class="alignright size-medium wp-image-76" title="proliant_microserver" src="http://blog.arrington.me/wp-content/uploads/proliant_microserver-300x175.jpg" alt="" width="300" height="175" srcset="https://blog.arrington.me/wp-content/uploads/proliant_microserver-300x175.jpg 300w, https://blog.arrington.me/wp-content/uploads/proliant_microserver.jpg 559w" sizes="(max-width: 300px) 100vw, 300px" />](http://blog.arrington.me/wp-content/uploads/proliant_microserver.jpg)drive. The Microserver line was recently updated with an AMD Turion II Model Neo N40L (previously used the N36L) dual core processor running at 1.5GHz, 2GB memory (8GB max), and a 250GB hard drive. HP intends this to be used in a small business environment with no more than 10 clients.

For the price, the Microserver is an amazing solution. Without an OS, this configuration is only $349. HP will also preinstall Windows Small Business Server 2011 Essentials and upgrade you to 2x 500GB HDD and 4GB RAM for a total of $899.

As attractive as the Microserver is, it is way too slow for my virtualization needs. While I could probably run one or two mostly idle VMs, doing anything more demanding would bring the system to a halt. If transcoding video was ever necessary, I suspect I might be able to do it by hand quicker. It is, however, perfect for a dedicated NAS with less demanding services (SSH, web, development).

SilentPCReview has a review of the Microserver [here](http://www.silentpcreview.com/HP_Proliant_MicroServer).

##### Pros

  * The no-OS option is very inexpensive.
  * The size is unbeatable for the features, can be thrown anywhere.
  * Has a large amount of USB ports for expandable storage with one internal port for a bootable flash drive.
  * One year warranty for parts.
  * Two PCIe expansion slots (x1 and x16)
  * Very low power consumption. Processor is rated at 12W TDP. Typical power usage with four drives is around 30-40W.
  * eSATA
  * Gigabit ethernet
  * Easy access to hard drives
  * Low noise (~24dB)

##### Cons

  * Some preconfigured options are laughable. Throw away the memory and tiny hard drive and lower the price, HP!
  * Slow, slow, slow processor
  * Swapping memory modules requires a lot of disassembly
  * Only supports 2TB drives or lower.
  * Lacks some virtualization features like VMDirectPath

### The Custom

Although I love the Microserver, it just doesn&#8217;t satisfy the urge to build a system from the ground up that all geeks have. You can always get exactly what you want and it&#8217;s usually cheaper. However, with smaller systems like this, it can be difficult to match the prices of preconfigured boxes but the processing power you gain is, in my opinion, completely worth it.

To start our build, we need something to put our components into. There is no better NAS computer case manufacturer than Lian Li. They have several small and awesome cases for not only NAS boxes but also HTPCs and full desktops. For my build, I am choosing the [Lian Li PC-Q25B](http://www.lian-li.com/v2/en/product/product06.php?pr_index=584&cl_index=1&sc_index=25&ss_index=64&g=f). I&#8217;m actually a little bummed that I didn&#8217;t know about the more portable suitcase style [Lian Li PC-TU200](http://www.lian-li.com/v2/en/product/product06.php?pr_index=585&cl_index=1&sc_index=25&ss_index=64) before I purchased the Q25B.

[<img class="alignleft size-full wp-image-81" title="Q25B" src="http://blog.arrington.me/wp-content/uploads/q25-02s.jpg" alt="" width="175" height="175" srcset="https://blog.arrington.me/wp-content/uploads/q25-02s.jpg 175w, https://blog.arrington.me/wp-content/uploads/q25-02s-150x150.jpg 150w" sizes="(max-width: 175px) 100vw, 175px" />](http://blog.arrington.me/wp-content/uploads/q25-02s.jpg)Anyways, the Q25B has five hot swappable, easy-to-access hard drive bays. It also has a plate that can fit three additional hard drives (2.5&#8243; or a combination 2.5&#8243; and 3.5&#8243;) for a total of eight hard drives in one tiny case. It has a large front intake fan that blows air right over the drives and plenty of ventilation going out. It fits Mini-ITX style motherboards and has enough room for an 11&#8243; graphics card if you so desire. You can also fit a standard ATX power supply. It does not, however, have any front USB ports like the TU200 but turns this in it&#8217;s favor in that the case is absolutely gorgeous with the flush front panel and simple style.

Time to put something in that fancy new case! Depending on your performance needs, you can go either low power and save some money or high power and save some time.

##### Low end

To meet our requirements as a storage system, it needs to have at least five SATA ports. To fit in our case it must be Mini-ITX. This limits our options quite a bit. It also needs to be very energy efficient. I believe I have found the perfect board for the low end NAS enthusiast: meet the [ASUS E35M1-I](http://www.newegg.com/Product/Product.aspx?Item=13-131-732&SortField=0&SummaryType=0&Pagesize=10&PurchaseMark=&SelectedRating=-1&VideoOnlyMark=False&VendorMark=&IsFeedbackTab=true&Page=2#scrollFullInfo).

This ASUS board is loaded with six SATA III ports. It has a soldered-on, built-in processor to reduce power consumption. It has a passive heatsink to reduce noise. It supports up to 8GB DDR3 memory and has a [really neat EFI BIOS](http://www.youtube.com/watch?v=9n93ZBtv5mg) that looks really modern.  For a CPU and motherboard together, at $120, it is hard to beat.

The processor is a dual core, 1.6GHz AMD E-350. The E-350 often performs better than the Intel Atom [D525](http://www.fudzilla.com/reviews/item/22841-amd-e350-e-350-intel-atom-d525-ion2/22841-amd-e350-e-350-intel-atom-d525-ion2). It is definitely more nimble than the Microserver&#8217;s N40L but it wouldn&#8217;t touch an i3. The payoff of energy savings is, however, huge. This board consumes less than 20 watts by itself.

For my VM requirement, it is lacking. If it was not for that, I would pick up this board in a heartbeat.

##### Higher End

In Q1 2011, Intel released it&#8217;s desktop [Sandy Bridge](http://en.wikipedia.org/wiki/Sandy_Bridge) chips. These processors give you a great performance-to-watts ratio with the 32nm manufacturing process. This seemed like the obvious choice.

For the higher-end build, I chose the Intel [i3-2100T](http://www.newegg.com/Product/Product.aspx?Item=N82E16819116394) and [i5-2400S](http://www.newegg.com/Product/Product.aspx?Item=N82E16819115075&Tpk=i5%202400s). These processors are the energy efficient cousins of the i3-2100 and i5-2400, respectively. The i3-2100T has a TDP of 35W while the i5-2400S has a TDP of 65W, each 30W lower than their cousins. Although they consume 30W less, their performance is not significantly diminished, [as shown on SilentPCReview](http://www.silentpcreview.com/intel-2100t-2400s).

After studying the ASUS board above so heavily, I became obsessed with picking a board with six SATA ports so I have room to expand later. As it turns out, Newegg only carried one Mini-ITX board that was Socket LGA 1155 and had 6 SATA ports: the [ZOTAC H67ITX-C-E](http://www.newegg.com/Product/Product.aspx?Item=N82E16813500064). I am a little apprehensive about choosing a board manufacturer I have never used before but I&#8217;m willing to give it a shot.

The first thing you will notice looking at this board is the strange and painfully obvious wires coming out of the wireless chip to the antenna connectors. It&#8217;s ugly but useful as this gives you a second NIC which could be beneficial for a VM box. It is 802.11n but is only on the 2.4GHz band, unfortunately. It has USB 3.0, supports 16GB memory, HDMI and Display Port, optical audio out. All very cool features and useful for a HTPC system as well! The reviews are a bit mixed about the quality, however.

###