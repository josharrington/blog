---
id: 134
title: 'Automating Google Hurdles 2012 &#8211; xautomation and xte'
date: 2012-08-07T20:40:50+00:00
author: josh
layout: post
guid: https://blog.arrington.me/?p=134
permalink: /2012/automating-google-hurdles-2012-xautomation-and-xte/
categories:
  - Gaming
tags:
  - athlete
  - cheating
  - games
  - google
  - hurdles
  - race
  - xautomation
  - xte
---
Today, Google&#8217;s home page had a [little game](https://www.google.com/doodles/hurdles-2012) where you help an athlete run and jump hurdles. A coworker and I were racing to try to beat each other. I wanted to win! With a little Linux know-how, it is fairly easy to automate the game.

[<img class="aligncenter size-full wp-image-135" title="Hurdles05s" src="https://blog.arrington.me/wp-content/uploads/Selection_009.png" alt="" width="532" height="231" /><!--more-->](https://blog.arrington.me/wp-content/uploads/Selection_009.png)

### xautomation and xte

[Xautomation](http://hoopajoo.net/projects/xautomation.html "xautomation") is a tool that allows the user to automate inputs that feed to the X server. In other words, you can create scripts or one-liners to move your mouse around, click, and type on your keyboard. The usage is fairly simple. The program we are interested in is called _xte_. It is part of the xautomation suite. It accepts commands as arguments or from stdin. Check out the [man page](http://linux.die.net/man/1/xte) A few simple examples:

<pre class="brush: bash; title: ; notranslate" title="">#Type "Hello World"
xte 'str Hello World'

#Type "Hello World" and press enter
xte 'str Hello World' 'key Return'

#Move mouse to position x:160,y:700 and left click
xte 'mousemove 160 700' 'mousedown 1' 'mouseup 1'

#Move mouse, click, drag, release from stdin
#When sending input from stdin, send each command on a new line without single quotes
echo -e "mousemove 500 500\nmousedown 1\nmousemove 700 500\nmouseup 1" | xte
</pre>

### The game (you lost it!)

The game requires you to press keys alternating between left and right to move your athlete. You then use the spacebar to jump over hurdles. The hurdles seem to show up at random intervals. To keep things simple, I will ignore the hurdles and just plow straight through them! My solution:

<pre class="brush: plain; title: ; notranslate" title="">sleep 5 && python -c "print 'key Left\nkey Right\n' * 1000" | xte
</pre>

I am using a small bit of python code to generate 1000 left-right key press pairs. This was faster on my machine than running `xte 'key Left' 'key Right'` in a bash loop. These are then fed to xte to run. This is delayed by 5 seconds to allow me to focus (click on) on my browser and the flash applet before it starts charging! I have noticed that because this completes the track so quickly, hurdles are often not generated and you can get 3 stars. It seems if you hit even one hurdle, your can only get up to two stars.

There are probably many ways to optimize this. My best time is 0.5 seconds with 3 stars using the above code. Can you beat it?