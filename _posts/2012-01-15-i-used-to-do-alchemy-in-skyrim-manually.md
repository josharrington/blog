---
id: 93
title: 'I used to do Alchemy in Skyrim manually&#8230;'
date: 2012-01-15T20:10:09+00:00
author: josh
layout: post
guid: http://blog.arrington.me/?p=93
permalink: /2012/i-used-to-do-alchemy-in-skyrim-manually/
categories:
  - Gaming
tags:
  - alchemy
  - skyrim
---
Then I made a calculator!

I didn&#8217;t care for any of the other alchemy calculators that were available so I made my own. I wanted something that would show me the most profitable path for the ingredients I owned. I also added a way to save your inventory for later use. [You can try it out here](http://arrington.me/alch/).

Many thanks to [this fine gentleman or gentlewoman](http://keke.itarium.ch/uploads/alchemy/) for your data. I wish I could have found a way to contact you!

I have not tested this in Internet Explorer. It should work in IE9 but any other IE version is unknown. Chrome and Firefox work flawlessly.

EDIT 10/11/2014: 

I made some updates so this will work much much better in the Steam browser. Check it out!