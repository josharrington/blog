---
id: 100
title: Change Your Skype Mood Text In Linux With Python
date: 2012-01-22T17:40:14+00:00
author: josh
layout: post
guid: http://blog.arrington.me/?p=100
permalink: /2012/change-your-skype-mood-text-in-linux-with-python/
categories:
  - Programming
tags:
  - linux
  - python
  - skype
  - wdlxtv
  - wdtv
---
I thought it would be neat to change my Skype mood text based on what I was watching on my WDTV. I have a [WDTV Live Plus](http://wdc.com/en/products/products.aspx?id=320) modded with [WDLXTV](http://wdlxtv.com/) (version 0.5.1.1 based on 1.05.04) that displays what I am watching on a web dashboard. To accomplish this, it is as simple as scraping the data from the dashboard and passing it to Skype using Skype&#8217;s API. On Linux, Skype uses [D-Bus](http://en.wikipedia.org/wiki/D-Bus) to interact with its API. The scripts I used are below.<!--more-->

**skype_mood.py**

<pre class="brush: python; title: ; notranslate" title="">#!/usr/bin/python
# script to change Skype mood status, usage: skype_mood.py new_mood

import dbus
import sys

def mood(text=""):
    bus = dbus.SessionBus()
    try:
        proxy = bus.get_object('com.Skype.API', '/com/Skype')
        proxy.Invoke('NAME skype_mood.py')
        proxy.Invoke('PROTOCOL 2')
        if text=="":
            command = 'GET PROFILE MOOD_TEXT'
        else:
            command = 'SET PROFILE MOOD_TEXT %s' % text
        return proxy.Invoke(command)
    except:
            print "Could not contact Skype client"

if __name__ == "__main__":
    if len(sys.argv) &gt;= 2:
        mood(sys.argv[1])
    else:
        print mood()
</pre>

**wdtvplaying_skypemood.py**

<pre class="brush: python; title: ; notranslate" title="">import urllib2
import re
from skype_mood import mood

wdlxtvurl = "http://WDLXTVURL/"
defaultmood = "Nothing Playing"

try:
    content = urllib2.urlopen(wdlxtvurl).read()
except:
    print "Could not reach server"
    mood(defaultmood)
    exit()

m = re.search('id="deviceStatus".*?&gt;(.*?)&lt;', content)
status = m.group(1).strip()

if status in ("PLAYING", "PAUSED_PLAYBACK"):
    m = re.search('id="currentMedia".*?&gt;(.*?)&lt;', content)
    playing = m.group(1).strip()
    print mood(playing)
else:
    mood(defaultmood)
</pre>

The first script, skype_mood.py, is a generic script. The second script imports the first one, grabs the relevant information from WDLXTV&#8217;s dashboard and if media is playing or paused, it will display the name or file name of the media as your mood status text in Skype. I had difficulty passing basic authentication credentials (urllib&#8217;s password manager or adding an authentication header) to my WDLXTV so I decided to remove the authentication altogether. If you want to run this with cron, you will need to configure some extra things [detailed here](http://earlruby.org/2008/08/update-pidgin-status-using-cron/).

### Resources

  * <http://www.goldb.org/pythonwebscraping.html>
  * <http://pastebin.com/AAeSHVEG>
  * <https://developer.skype.com/public-api-reference#OBJECTS>
  * <http://wiki.wdlxtv.com/NoPasswordApache>
  * <http://earlruby.org/2008/08/update-pidgin-status-using-cron/>