---
id: 167
title: Automatically Uploading Torrents to Usenet
date: 2014-04-21T12:42:09+00:00
author: josh
layout: post
guid: https://blog.arrington.me/?p=167
permalink: /2014/automatically-uploading-torrents-to-usenet/
categories:
  - Uncategorized
---
I&#8217;ve created a docker project that will automate the downloading and uploading of torrents from RSS feeds to usenet. See my [Github page](https://github.com/josharrington/docker-usenetter) for more info.