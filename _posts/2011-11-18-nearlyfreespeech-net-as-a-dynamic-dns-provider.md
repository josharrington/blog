---
id: 6
title: NearlyFreeSpeech.NET as a dynamic DNS provider
date: 2011-11-18T20:40:40+00:00
author: josh
layout: post
guid: http://blog.arrington.me/?p=6
permalink: /2011/nearlyfreespeech-net-as-a-dynamic-dns-provider/
categories:
  - Networking
tags:
  - dynamic dns
  - nearlyfreespeech.net
---
Dynamic DNS services provide great value to people who wish to host their own services at home or any place without a static IP address. Providers include DynDNS and No-Ip. However, for free they only give you a subdomain of their own site (e.g. MySite.no-ip.org). They will, however, host your domain&#8217;s DNS services but if you are like me, you already have your own domain and DNS hosting that probably came with your web host.

I use NearlyFreeSpeech to host my website and DNS and I wanted to use that DNS service to give my home servers a name on the internet. [NearlyFreeSpeech has an API](https://members.nearlyfreespeech.net/wiki/API/Introduction)(requires login) which you can use to make changes to your account in creative ways. I have only seen one other article about this topic that used perl and some CPAN modules ([here](http://www.lingams.net/?p=118)). I created a method for using PHP and curl instead so I didn&#8217;t need to install anything extra. Here&#8217;s how you do it:<!--more-->

  1. Register for an API key for your NearlyFreeSpeech.net account. Fill out a support request and they will send you one.
  2. Login to your NFS SSH account and open a text editor `<br />
nano /home/public/dynamicdns.php`
  
    Copy and paste the following into your text editor and save:</p> <pre class="brush: php; title: ; notranslate" title="">&lt;?php
require_once("NFSN/API/Manager.php");

//CONFIGURE THIS STUFF
$strLogin = "MYUSERNAME";
$strAPIKey = "MYAPIKEY";
$subdomain = "MYSUBDOMAIN";  //Example: home , work , grandma
$domain = "MYDOMAIN";  //Example: google.com , mydomain.com , school.org

$bDebug = false;
$api = new APIManager($strLogin, $strAPIKey, $bDebug);
$dns = $api-&gt;NewDNS($domain);
$entries = $dns-&gt;ListRRs($subdomain);
$old = "";
$new = $_SERVER['REMOTE_ADDR'];

$entries = split(",", $entries);
$old = split(":", $entries[2]);
$old = str_replace("\"", "", $old[1]);

//Check that the script has your IP address
if ($new != "")
{
    //Remove the old DNS entry for the subdomain if it exists
    if ($old != "")
    {
        $dns-&gt;RemoveRR($subdomain, "A", $old);
    }

    //Add new subdomain DNS entry
    $dns-&gt;AddRR($subdomain, "A", $new);
    print "$new\nSuccess";
}
else
{
    print "$new\nFailed";
}
?&gt;
</pre>

  3. Edit the variables near the top to match your situation. strLogin should be the username that you use to login to the NFS.NET website and not the one you use to SSH into your hosted site. strAPIkey should be the API key that you received from support in the first step. Subdomain should be the name you want your subdomain to be. Domain should be the domain you are having hosted by NFS (in my case, arrington.me). Save the file.
  4. Test it. Navigate to your site using your web browser (http://mydomain.com/dynamicdns.php). If all goes well, you should see your current public IP address and the word &#8220;Success.&#8221; You will be able to see your IP address as a new &#8220;A&#8221; resource record in your account on NearlyFreeSpeech.Net.

### Securing the Script

So now you have a script that works, right? Great! But we don&#8217;t want just anybody to access it and change the IP address and potentially create a MITM attack. We can use Apache&#8217;s basic authentication to force you to enter a username and password before the script will be run.

  1. Where you saved your dynamicdns.php script, create a .htaccess file.
  
    `nano /home/public/.htaccess`
  
    Add the following:</p> <pre class="brush: plain; title: ; notranslate" title="">AuthUserFile /PATH/TO/.HTPASSWD
AuthType Basic
AuthName "dns"

  Require valid-user

</pre>

  2. We now want to create a .htpasswd file to store our user credentials. I stored mine in my /home/protected/ directory. Because NFS.NET does some weird gymnastics to show protect users and alias file paths, you will need to use the full path that Apache uses to find your .htpasswd file. Login to your NFS.NET account and click on &#8220;Sites&#8221; and then the &#8220;Short Name&#8221; for the site you are setting this up on. Towards the bottom you will find &#8220;Apache Site Root.&#8221; It will look like `/d6/sitename/`
  3. Open .htaccess file that we created above and edit the AuthUserFile line to reflect your apache site root discovery. Example: `AuthUserFile /d6/sitename/protected/.htpasswd`
  4. To create your .htpasswd file, you will need a username and password you want to use to protect your script. Use whatever you want for these but I recommend using a VERY strong password because it IS accessible via the internet and because you will not need to remember it if you automate the process (described below). You can use [GRC&#8217;s Ultra High Security Passwords page](https://www.grc.com/passwords.htm) to generate a very high quality password. You may even consider using it to generate a username too! You may want to copy it to a text editor temporarily while we configure some stuff.
  5. While SSHed into your NFS.NET site, run the following command, replacing MYUSERNAME with the username you chose:
  
    `htpasswd -c -s /home/protected/.htpasswd MYUSERNAME`
  
    It will prompt you for your new password twice. You may want to copy and paste in the password you saved from GRC&#8217;s website. It will create the file and add the user credentials to it. It will also hash the password using SHA which should be sufficient for this use and if you used a very long password. Your .htaccess file will look something like this:</p> <pre class="brush: plain; title: ; notranslate" title="">MYUSERNAME:{SHA}A95sVwv+JL/DKMzXyka3bq2vQzQ=</pre>

  6. Visit your website again (http://mydomain.com/dynamicdns.php). It should prompt you for your credentials. Try inputting something that would fail to test that the authentication is working properly. Then test the correct credentials you used above. You should get the same page as before with your IP address and the word &#8220;Success.&#8221;

### Automating the Process; Final Thoughts

Your dynamic IP address from your ISP is never guaranteed to be the same. To set up automatic renewal, I set up curl to hit the script every so often via cron on my Linux servers or on my DD-WRT router. You can use a line like this for cron:

<pre class="brush: plain; title: ; notranslate" title="">30 5 * * * root "curl http://mydomain.com/dynamicdns.php --user MYUSERNAME:MYPASSWORD"</pre>

This will make the NFS.NET web server run your new script at 5:30am every day. For information about how to use cron, see my resources section.

This is the best solution I have found for having my own, (very) inexpensive DNS hosting and domain with dynamic DNS for my hosts. Typically, I would set up a new script for each host that I wanted to give a subdomain (e.g. office.php for office.mydomain.com, grandma.php for grandma.mydomain.com, etc). In theory, you could set up a single script to manage all of these by passing your intended subdomain as a POST parameter with curl.

It should be noted that if you are not using SSL for your web hosting (NFS.NET does not offer this currently), your password and username can be intercepted when passing over the network. This is not intended to be a super secure solution, just a simple and inexpensive one. If security is a concern, you may consider using the perl and CPAN method linked above.

### Resources

  * Intro to cron: <http://unixgeeks.org/security/newbie/unix/cron-1.html>
  * Cron on DD-WRT: <http://www.dd-wrt.com/wiki/index.php/CRON>