---
id: 66
title: About
date: 2011-12-06T13:04:39+00:00
author: josh
layout: page
guid: http://blog.arrington.me/?page_id=66
---
My name is Joshua Arrington and I am a systems engineer and programming professional in North Texas. I have an insatiable curiosity when it comes to all things technology.

This blog is my personal journal of my projects and other thoughts. I have many years of experience in Linux, Windows, .NET, Python, and Config Management.